import java.lang.reflect.*;

public class DumpMethods{

	public static void main(String[] args){
		try{
			Class c = Class.forName(args[0]);
			recPrintMethods(c);
		}catch(Exception e){e.printStackTrace();}

	}

	public static void recPrintMethods(Class c){
		for(Method x : c.getDeclaredMethods()){
			System.out.println(x.toString());
		}
		Class<?> father = c.getSuperclass();
		if(father!=null)
			recPrintMethods(father);

	}



}