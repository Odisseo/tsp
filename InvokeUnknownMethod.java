import java.lang.reflect.*;
import java.lang.Integer;
import java.lang.Double;
import java.util.regex.Pattern;
import java.util.Arrays;
import java.util.ArrayList;


public class InvokeUnknownMethod{

	public static void main(String[] args){

		try{

			Class c = Class.forName(args[0]);
			if(args[1]==null){
				System.out.println("No Method invoked");
				return;	
			}

			String method = args[1];
			Method called=null;
			for(Method x : c.getDeclaredMethods()){
				if(x.getName().equals(args[1]))
					called = x;
			}
			if(called==null){
				System.out.println("Cannot find method");
			}
			ArrayList<Object> arglist = new ArrayList(); 
			int arg_count = 2;
			for(Type t : called.getGenericParameterTypes()){
				String type = t.toString();
				if(args.length<=arg_count){
					System.out.println("no enough params");
					return;
				}
				if(type.equals("int")){
					arglist.add((Object) Integer.parseInt(args[arg_count]));
				}
				if(type.equals("double")){
					arglist.add((Object) Double.parseDouble(args[arg_count]));
				}


				arg_count++;
			}
			Object returned = called.invoke(c.newInstance(),arglist.toArray());
			System.out.println(returned.toString());


		}catch(Exception e){e.printStackTrace();}

	}


}