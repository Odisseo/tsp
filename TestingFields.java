import java.util.*;
import java.lang.*;
import java.lang.reflect.*;
import java.util.regex.Pattern;


public class TestingFields {
	private Double d[];
	private Date dd;
	public static final int i = 42;
	protected static String s = "testing ...";
	private ArrayList<String> my_test = new ArrayList<>(Arrays.asList("The","Expanse"));

	public TestingFields(int n, double val) {
		dd = new Date();
		d = new Double[n];
		for(int i=0; i<n; i++) d[i] = i*val;

	}

	public static void main(String[] args){

		TestingFields t = new TestingFields(7,3.14);

		Class c = TestingFields.class;
		try{
			for(Field f : c.getDeclaredFields()){
				/* OPS: ecco quando non leggi la doc bene
				Class field_class = f.getDeclaringClass();
				if(Arrays.asList(field_class.getInterfaces()).contains(Iterable.class)){
					System.out.println(f);
					for(Object x: (Iterable) f.get(t))
						System.out.println("\t"+x);
				}
				*/

				if(f.get(t) instanceof Object[]){
					System.out.println(f.getType()+" "+f.getName());
					for(Object x: (Object[]) f.get(t)){
						System.out.println("\t"+x);
					}
				}
				else{
					System.out.println(f.getType()+" "+f.getName()+" = "+f.get(t));
				}
			}
			Field test = c.getDeclaredField("s");
			if(test!=null){
				test.set(t,test.get(t)+" passed!!");
				System.out.println(test.get(t));
			}


		}catch(Exception e){e.printStackTrace();}

	}
}


